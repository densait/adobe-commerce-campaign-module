Adobe Campaign module
====================

by Dzianis Yurevich

[gitlab link](https://gitlab.com/densait/adobe-commerce-campaign-module)

### Description
Adobe Campaign module for Magento 2.4.6

###Installation

- Please add the GitLab repo to your Magento composer.json file:
```
    "repositories": [
        ....,
        {
            "type": "vcs",
            "url": "https://gitlab.com/densait/adobe-commerce-campaign-module.git"
        }
    ],
```

- Install the module via Composer:
```
    composer require denis/campaign dev-main
```

- Run the setup:upgrade command:
```
    php bin/magento setup:upgrade
```

- Reindex Catalog Search data:
```
    php bin/magento indexer:reindex catalogsearch_fulltext
```

- Create your first Campaign in the Magento BackOffice:
```
    Marketing -> Campaigns -> Manage Campaigns
```
