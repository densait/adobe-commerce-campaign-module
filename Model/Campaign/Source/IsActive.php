<?php

namespace Denis\Campaign\Model\Campaign\Source;

use Denis\Campaign\Api\Data\CampaignInterface;
use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class IsActive
 */
class IsActive implements OptionSourceInterface
{
    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $availableOptions = $this->getAvailableStatuses();
        $options = [];
        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }
        return $options;
    }

    /**
     * @return array
     */
    public function getAvailableStatuses()
    {
        return [
            CampaignInterface::STATUS_ENABLED => __('Enabled'),
            CampaignInterface::STATUS_DISABLED => __('Disabled')
        ];
    }
}
