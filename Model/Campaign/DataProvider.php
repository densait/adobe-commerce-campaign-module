<?php

namespace Denis\Campaign\Model\Campaign;

use Denis\Campaign\Api\CampaignRepositoryInterface;
use Denis\Campaign\Api\Data\CampaignInterface;
use Denis\Campaign\Model\CampaignFactory;
use Denis\Campaign\Model\ResourceModel\Campaign\CollectionFactory;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Ui\DataProvider\Modifier\PoolInterface;
use Magento\Ui\DataProvider\ModifierPoolDataProvider;

/**
 * Cms Page DataProvider
 */
class DataProvider extends ModifierPoolDataProvider
{
    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var array
     */
    protected $loadedData;

    /**
     * @var CampaignRepositoryInterface
     */
    protected $campaignRepository;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var CampaignFactory
     */
    protected $campaignFactory;

    /**
     * DataProvider constructor.
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param DataPersistorInterface $dataPersistor
     * @param RequestInterface $request
     * @param CampaignRepositoryInterface $campaignRepository
     * @param CampaignFactory $campaignFactory
     * @param array $meta
     * @param array $data
     * @param PoolInterface|null $pool
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $campaignCollectionFactory,
        DataPersistorInterface $dataPersistor,
        RequestInterface $request,
        CampaignRepositoryInterface $campaignRepository,
        CampaignFactory $campaignFactory,
        array $meta = [],
        array $data = [],
        PoolInterface $pool = null
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data, $pool);
        $this->collection = $campaignCollectionFactory->create();
        $this->dataPersistor = $dataPersistor;
        $this->meta = $this->prepareMeta($this->meta);
        $this->request = $request;
        $this->campaignRepository = $campaignRepository;
        $this->campaignFactory = $campaignFactory;
    }

    /**
     * Prepares Meta
     *
     * @param array $meta
     * @return array
     */
    public function prepareMeta(array $meta)
    {
        return $meta;
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }

        $campaign = $this->getCurrentCampaign();
        $this->loadedData[$campaign->getId()] = $campaign->getData();
        return $this->loadedData;
    }

    /**
     * Return current campaign
     *
     * @return CampaignInterface
     */
    private function getCurrentCampaign()
    {
        $campaignId = $this->getCampaignId();
        if ($campaignId) {
            try {
                $campaign = $this->campaignRepository->getById($campaignId);
            } catch (LocalizedException $exception) {
                $campaign = $this->campaignFactory->create();
            }

            return $campaign;
        }

        $data = $this->dataPersistor->get('campaign');
        if (empty($data)) {
            return $this->campaignFactory->create();
        }

        $this->dataPersistor->clear('campaign');
        return $this->campaignFactory->create()
            ->setData($data);
    }

    /**
     * Returns current campaign id from request
     *
     * @return int
     */
    private function getCampaignId()
    {
        return (int)$this->request->getParam($this->getRequestFieldName());
    }
}
