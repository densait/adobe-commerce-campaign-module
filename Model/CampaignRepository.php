<?php

namespace Denis\Campaign\Model;

use Denis\Campaign\Api\Data\CampaignInterface;
use Denis\Campaign\Api\Data\CampaignInterfaceFactory;
use Denis\Campaign\Api\Data\CampaignSearchResultsInterface;
use Denis\Campaign\Api\Data\CampaignSearchResultsInterfaceFactory;
use Denis\Campaign\Api\CampaignRepositoryInterface;
use Denis\Campaign\Model\ResourceModel\Campaign as ResourceCampaign;
use Denis\Campaign\Model\ResourceModel\Campaign\CollectionFactory as CampaignCollectionFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Campaign repository
 */
class CampaignRepository implements CampaignRepositoryInterface
{
    /**
     * @var ResourceCampaign
     */
    protected $resource;

    /**
     * @var CampaignInterfaceFactory
     */
    protected $campaignFactory;

    /**
     * @var CampaignCollectionFactory
     */
    protected $campaignCollectionFactory;

    /**
     * @var CampaignSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var CollectionProcessorInterface
     */
    protected $collectionProcessor;

    /**
     * @param ResourceCampaign $resource
     * @param CampaignInterfaceFactory $campaignFactory
     * @param CampaignCollectionFactory $campaignCollectionFactory
     * @param CampaignSearchResultsInterfaceFactory $searchResultsFactory
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        ResourceCampaign $resource,
        CampaignInterfaceFactory $campaignFactory,
        CampaignCollectionFactory $campaignCollectionFactory,
        CampaignSearchResultsInterfaceFactory $searchResultsFactory,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->resource = $resource;
        $this->campaignFactory = $campaignFactory;
        $this->campaignCollectionFactory = $campaignCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * Save Campaign data
     *
     * @param CampaignInterface $campaign
     * @return CampaignInterface
     * @throws CouldNotSaveException
     */
    public function save(CampaignInterface $campaign)
    {
        try {
            if ($campaign->getStoreId() === null) {
                $storeId = $this->storeManager->getStore()->getId();
                $campaign->setStoreId($storeId);
            }
            $this->resource->save($campaign);
            $this->resource->updateStores($campaign->getId(), $campaign->getStoreId());
            $this->resource->updateRelatedProducts($campaign->getId(), $campaign->getRelatedProductIds());
        } catch (LocalizedException $exception) {
            throw new CouldNotSaveException(
                __('Could not save the campaign: %1', $exception->getMessage()),
                $exception
            );
        } catch (\Throwable $exception) {
            throw new CouldNotSaveException(
                __('Could not save the campaign: %1', __('Something went wrong while saving the campaign.')),
                $exception
            );
        }
        return $campaign;
    }

    /**
     * Load Campaign data by given Campaign ID
     *
     * @param int $campaignId
     * @return CampaignInterface
     * @throws NoSuchEntityException
     */
    public function getById($campaignId)
    {
        $campaign = $this->campaignFactory->create();
        $campaign->load($campaignId);
        if (!$campaign->getId()) {
            throw new NoSuchEntityException(__('The Campaign with the "%1" ID doesn\'t exist.', $campaignId));
        }
        return $campaign;
    }

    /**
     * Load campaigns collection by given search criteria
     *
     * @param SearchCriteriaInterface $criteria
     * @return CampaignSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $criteria)
    {
        $collection = $this->campaignCollectionFactory->create();
        $this->collectionProcessor->process($criteria, $collection);
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * Delete Campaign
     *
     * @param CampaignInterface $campaign
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(CampaignInterface $campaign)
    {
        try {
            $this->resource->delete($campaign);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(
                __('Could not delete the campaign: %1', $exception->getMessage())
            );
        }
        return true;
    }

    /**
     * Delete campaign by given campaign ID
     *
     * @param int $campaignId
     * @return bool
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById($campaignId)
    {
        return $this->delete($this->getById($campaignId));
    }

    /**
     * @param int[] $productIds
     * @param bool|null $isActive
     * @param int|null $storeId
     * @return CampaignInterface[]
     */
    public function getCampaignsByProductIds($productIds, $isActive = null, $storeId = null)
    {
        $collection = $this->campaignCollectionFactory->create();
        $collection->filterCampaignsByProductIds($productIds, $isActive, $storeId);
        return $collection->getItems();
    }
}
