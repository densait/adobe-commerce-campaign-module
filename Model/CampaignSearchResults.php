<?php

namespace Denis\Campaign\Model;

use Denis\Campaign\Api\Data\CampaignSearchResultsInterface;
use Magento\Framework\Api\SearchResults;

/**
 * Service Data Object with Page search results.
 */
class CampaignSearchResults extends SearchResults implements CampaignSearchResultsInterface
{
}
