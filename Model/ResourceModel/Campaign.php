<?php

namespace Denis\Campaign\Model\ResourceModel;

use Denis\Campaign\Api\Data\CampaignInterface;
use Magento\Framework\DB\Select;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Campaign mysql resource
 */
class Campaign extends AbstractDb
{
    /**
     * MySQL Table Name
     */
    const TABLE_NAME = 'denis_campaign';
    const RELATED_PRODUCT_TABLE_NAME = 'denis_campaign_product';
    const RELATED_STORE_TABLE_NAME = 'denis_campaign_store';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(self::TABLE_NAME, CampaignInterface::CAMPAIGN_ID);
    }

    /**
     * Update campaign stores
     *
     * @param string $campaignId
     * @param array|int $newStores
     * @return void
     * @throws LocalizedException
     */
    public function updateStores($campaignId, $newStores)
    {
        if (null === $newStores) {
            return;
        }

        $connection = $this->getConnection();
        $oldStores = $this->getStoreIds($campaignId);
        $storeTable = $this->getTable(self::RELATED_STORE_TABLE_NAME);
        if (!is_array($newStores)) {
            $newStores = [$newStores];
        }

        $delete = array_diff($oldStores, $newStores);
        if ($delete) {
            $where = [
                'campaign_id = ?' => $campaignId,
                'store_id IN (?)' => $delete,
            ];
            $connection->delete($storeTable, $where);
        }

        $insert = array_diff($newStores, $oldStores);
        if ($insert) {
            $data = [];
            foreach ($insert as $storeId) {
                $data[] = [
                    'campaign_id' => (int)$campaignId,
                    'store_id' => (int)$storeId,
                ];
            }
            $connection->insertMultiple($storeTable, $data);
        }
    }

    /**
     * Update campaign products
     *
     * @param string $campaignId
     * @param array|int $newStores
     * @return void
     * @throws LocalizedException
     */
    public function updateRelatedProducts($campaignId, $newProductIds)
    {
        if (null === $newProductIds) {
            return;
        }

        $connection = $this->getConnection();
        $oldProductIds = $this->getRelatedProductIds($campaignId);
        $productTable = $this->getTable(self::RELATED_PRODUCT_TABLE_NAME);
        if (!is_array($newProductIds)) {
            $newProductIds = [$newProductIds];
        }

        $delete = array_diff($oldProductIds, $newProductIds);
        if ($delete) {
            $where = [
                'campaign_id = ?' => $campaignId,
                'product_id IN (?)' => $delete,
            ];
            $connection->delete($productTable, $where);
        }

        $insert = array_diff($newProductIds, $oldProductIds);
        if ($insert) {
            $data = [];
            foreach ($insert as $productId) {
                $data[] = [
                    'campaign_id' => (int)$campaignId,
                    'product_id' => (int)$productId,
                ];
            }
            $connection->insertMultiple($productTable, $data);
        }
    }

    /**
     * Get store ids to which specified item is assigned
     *
     * @param int $campaignId
     * @return array
     * @throws LocalizedException
     */
    public function getStoreIds($campaignId)
    {
        $select = $this->getStoreRelationSelect();
        $select->where('main.campaign_id = :campaign_id');
        return $this->getConnection()->fetchCol($select, ['campaign_id' => $campaignId]);
    }

    /**
     * @return Select
     * @throws LocalizedException
     */
    protected function getStoreRelationSelect()
    {
        $connection = $this->getConnection();
        $select = $connection->select()
            ->from(['cst' => self::RELATED_STORE_TABLE_NAME], 'store_id')
            ->join(
                ['main' => $this->getMainTable()],
                'cst.campaign_id = main.campaign_id',
                ['main.campaign_id']
            );
        return $select;
    }

    /**
     * @param int $campaignId
     * @param bool $asPairs
     * @return array
     */
    public function getRelatedProductIds($campaignId, $asPairs = false)
    {
        $connection = $this->getConnection();
        $select = $connection->select()
            ->from(
                ['relation' => $this->getTable(self::RELATED_PRODUCT_TABLE_NAME)],
                ['product_id', new \Zend_Db_Expr('"0" as position')]
            )
            ->where('relation.campaign_id = :campaign_id');

        if ($asPairs) {
            return $connection->fetchPairs(
                $select,
                ['campaign_id' => (int)$campaignId]
            );
        }

        return $connection->fetchCol(
            $select,
            ['campaign_id' => (int)$campaignId]
        );
    }

    /**
     * @param AbstractModel $object
     * @return Campaign
     * @throws LocalizedException
     */
    protected function _afterLoad(AbstractModel $object)
    {
        $campaign = parent::_afterLoad($object);
        $object->setStoreId($this->getStoreIds($object->getId()));
        $object->setRelatedProductIds($this->getRelatedProductIds($object->getId()));
        return $campaign;
    }
}
