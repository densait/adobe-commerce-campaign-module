<?php

namespace Denis\Campaign\Model\ResourceModel\Campaign;

use Denis\Campaign\Api\Data\CampaignInterface;
use Denis\Campaign\Model\Campaign;
use Denis\Campaign\Model\ResourceModel\Campaign as CampaignResource;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Store\Model\Store;

/**
 * Campaign collection
 */
class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = CampaignInterface::CAMPAIGN_ID;

    /**
     * Event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'campaign_collection';

    /**
     * Event object
     *
     * @var string
     */
    protected $_eventObject = 'campaign_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            Campaign::class,
            CampaignResource::class
        );
    }

    /**
     * @param int[] $productIds
     * @param bool|null $isActive
     * @param int|null $storeId
     * @return $this
     */
    public function filterCampaignsByProductIds($productIds, $isActive = null, $storeId = null)
    {
        $this->getSelect()
            ->joinInner(
                ['rel' => CampaignResource::RELATED_PRODUCT_TABLE_NAME],
                'main_table.campaign_id = rel.campaign_id',
                []
            );

        $this->getSelect()->where('rel.product_id IN (?)', $productIds);
        if (null !== $isActive) {
            $this->getSelect()->where('main_table.is_active = ?', (int)$isActive);
        }

        if (null !== $storeId) {
            $this->getSelect()
                ->joinInner(
                    ['store' => CampaignResource::RELATED_STORE_TABLE_NAME],
                    'main_table.campaign_id = store.campaign_id',
                    []
                );

            $this->getSelect()->where('store.store_id IN (?)', [$storeId, Store::DEFAULT_STORE_ID]);
        }

        return $this;
    }
}
