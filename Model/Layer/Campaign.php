<?php

namespace Denis\Campaign\Model\Layer;

use Denis\Campaign\Api\Data\CampaignInterface;
use Magento\Catalog\Model\Layer\Category;
use Magento\Catalog\Model\ResourceModel\Product\Collection;

class Campaign extends Category
{
    const LAYER_NAME = 'campaign';

    /**
     * Retrieve current layer product collection
     *
     * @return Collection
     */
    public function getProductCollection()
    {
        $campaign = $this->getCurrentCampaign();
        if (isset($this->_productCollections[$campaign->getId()])) {
            $collection = $this->_productCollections[$campaign->getId()];
        } else {
            $collection = $this->collectionProvider
                ->getCollection($this->getCurrentCategory());

            $collection->addFieldToFilter('entity_id', $campaign->getRelatedProductIds() ?: [0]);
            $this->prepareProductCollection($collection);
            $this->_productCollections[$campaign->getId()] = $collection;
        }

        return $collection;
    }

    /**
     * @return CampaignInterface
     */
    public function getCurrentCampaign()
    {
        $campaign = $this->_getData(self::LAYER_NAME);
        if ($campaign === null && ($campaign = $this->registry->registry('current_campaign'))) {
            $this->setData(self::LAYER_NAME, $campaign);
        }
        return $campaign;
    }
}
