<?php

namespace Denis\Campaign\Observer\UrlRewrite;

use Denis\Campaign\Model\Campaign;
use Denis\Campaign\Model\Campaign\UrlRewriteGenerator;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magento\UrlRewrite\Model\Exception\UrlAlreadyExistsException;
use Magento\UrlRewrite\Model\UrlPersistInterface;
use Magento\UrlRewrite\Service\V1\Data\UrlRewrite;

class ProcessCampaignSaveObserver implements ObserverInterface
{
    /**
     * @var UrlRewriteGenerator
     */
    protected $urlRewriteGenerator;

    /**
     * @var UrlPersistInterface
     */
    protected $urlPersist;

    /**
     * @param UrlRewriteGenerator $urlRewriteGenerator
     * @param UrlPersistInterface $urlPersist
     */
    public function __construct(
        UrlRewriteGenerator $urlRewriteGenerator,
        UrlPersistInterface $urlPersist
    ) {
        $this->urlRewriteGenerator = $urlRewriteGenerator;
        $this->urlPersist = $urlPersist;
    }

    /**
     * Generate urls for UrlRewrite and save it in storage
     *
     * @param EventObserver $observer
     * @return void
     * @throws UrlAlreadyExistsException
     */
    public function execute(EventObserver $observer)
    {
        /** @var $campaign Campaign */
        $campaign = $observer->getEvent()->getObject();
        if ($campaign->dataHasChangedFor('url_key')
            || $campaign->dataHasChangedFor('store_id')
        ) {
            $urls = $this->urlRewriteGenerator->generate($campaign);
            $this->urlPersist->deleteByData([
                UrlRewrite::ENTITY_ID => $campaign->getId(),
                UrlRewrite::ENTITY_TYPE => UrlRewriteGenerator::ENTITY_TYPE,
            ]);
            $this->urlPersist->replace($urls);
        }
    }
}
