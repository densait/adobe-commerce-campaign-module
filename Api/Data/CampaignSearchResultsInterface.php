<?php

namespace Denis\Campaign\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface for campaign search results.
 * @api
 */
interface CampaignSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get campaigns list.
     *
     * @return CampaignInterface[]
     */
    public function getItems();

    /**
     * Set campaigns list.
     *
     * @param CampaignInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
