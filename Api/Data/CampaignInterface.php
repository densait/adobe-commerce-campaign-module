<?php

namespace Denis\Campaign\Api\Data;

/**
 * Campaign interface.
 *
 * @method CampaignInterface setStoreId($storeId)
 * @method int|array getStoreId()
 * @method CampaignInterface setRelatedProductIds(array $relatedProductIds)
 * @method array getRelatedProductIds()
 * @api
 */
interface CampaignInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const CAMPAIGN_ID              = 'campaign_id';
    const IS_ACTIVE                = 'is_active';
    const TITLE                    = 'title';
    const DESCRIPTION              = 'description';
    const URL_KEY                  = 'url_key';
    /**#@-*/

    /**
     * Campaign's Statuses
     */
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Is active
     *
     * @return bool|null
     */
    public function isActive();

    /**
     * Get title
     *
     * @return string|null
     */
    public function getTitle();

    /**
     * Get description
     *
     * @return string|null
     */
    public function getDescription();

    /**
     * Get url key
     *
     * @return string|null
     */
    public function getUrlKey();

    /**
     * Set ID
     *
     * @param int $id
     * @return CampaignInterface
     */
    public function setId($id);

    /**
     * Set is active
     *
     * @param int|bool $isActive
     * @return CampaignInterface
     */
    public function setIsActive($isActive);

    /**
     * Set title
     *
     * @param string $title
     * @return CampaignInterface
     */
    public function setTitle($title);

    /**
     * Set description
     *
     * @param string $description
     * @return CampaignInterface
     */
    public function setDescription($description);

    /**
     * Set url key
     *
     * @param string $urlKey
     * @return CampaignInterface
     */
    public function setUrlKey($urlKey);
}
