<?php

namespace Denis\Campaign\Api;

use Denis\Campaign\Api\Data\CampaignInterface;
use Denis\Campaign\Api\Data\CampaignSearchResultsInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Campaign CRUD interface.
 * @api
 */
interface CampaignRepositoryInterface
{
    /**
     * Save campaign.
     *
     * @param CampaignInterface $campaign
     * @return CampaignInterface
     * @throws LocalizedException
     */
    public function save(CampaignInterface $campaign);

    /**
     * Retrieve campaign.
     *
     * @param int $campaignId
     * @return CampaignInterface
     * @throws LocalizedException
     */
    public function getById($campaignId);

    /**
     * Retrieve campaigns matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @return CampaignSearchResultsInterface
     * @throws LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * Delete campaign.
     *
     * @param CampaignInterface $campaign
     * @return bool true on success
     * @throws LocalizedException
     */
    public function delete(CampaignInterface $campaign);

    /**
     * Delete campaign by ID.
     *
     * @param int $campaignId
     * @return bool true on success
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function deleteById($campaignId);

    /**
     * @param int[] $productIds
     * @param bool|null $isActive
     * @param int|null $storeId
     * @return CampaignInterface[]
     */
    public function getCampaignsByProductIds($productIds, $isActive = null, $storeId = null);
}
