<?php

namespace Denis\Campaign\Block\Adminhtml\Campaign\Edit\Tab\RelatedProducts;

use Denis\Campaign\Api\CampaignRepositoryInterface;
use Denis\Campaign\Model\Campaign;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Grid\Column;
use Magento\Backend\Block\Widget\Grid\Extended;
use Magento\Backend\Helper\Data;
use Magento\Catalog\Model\ResourceModel\Product\Collection as ProductCollection;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Registry;
use Magento\Store\Model\WebsiteFactory;

/**
 * Class Grid
 */
class Grid extends Extended
{
    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var CampaignRepositoryInterface
     */
    protected $campaignRepository;

    /**
     * @var Visibility
     */
    protected $visibility;

    /**
     * @var WebsiteFactory
     */
    protected $websiteFactory;

    /**
     * @var Status
     */
    protected $status;

    /**
     * Grid constructor.
     * @param Context $context
     * @param Data $backendHelper
     * @param CollectionFactory $collectionFactory
     * @param Registry $registry
     * @param CampaignRepositoryInterface $campaignRepository
     * @param Visibility $visibility
     * @param WebsiteFactory $websiteFactory
     * @param Status $status
     * @param array $data
     */
    public function __construct(
        Context $context,
        Data $backendHelper,
        CollectionFactory $collectionFactory,
        Registry $registry,
        CampaignRepositoryInterface $campaignRepository,
        Visibility $visibility,
        WebsiteFactory $websiteFactory,
        Status $status,
        array $data = []
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->registry = $registry;
        $this->campaignRepository = $campaignRepository;
        $this->visibility = $visibility;
        $this->websiteFactory = $websiteFactory;
        $this->status = $status;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * Get Grid url
     *
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('denis_campaign/campaign/products', ['_current' => true]);
    }

    /**
     * @return array
     */
    protected function getSelectedProducts()
    {
        if ($this->getCampaign()->getId()) {
            return $this->getCampaign()->getRelatedProductIds();
        }
        return [];
    }

    /**
     * @return Campaign
     */
    protected function getCampaign()
    {
        return $this->registry->registry('campaign');
    }

    /**
     * Get replated products
     *
     * @param bool $assignedOnly
     * @return ProductCollection
     */
    public function getRelatedProducts($assignedOnly = true)
    {
        $collection = $this->collectionFactory->create();
        if ($this->getCampaign()->getId()) {
            $joinName = ['rel' => \Denis\Campaign\Model\ResourceModel\Campaign::RELATED_PRODUCT_TABLE_NAME];
            $expression = new \Zend_Db_Expr(
                'rel.product_id = e.entity_id AND rel.campaign_id = ' . $this->getCampaign()->getId()
            );
            if ($assignedOnly === true) {
                $collection->getSelect()->joinInner($joinName, $expression);
            } else {
                $collection->getSelect()->joinLeft($joinName, $expression);
            }
        }
        return $collection;
    }

    /**
     * @return void
     * @throws FileSystemException
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('campaign_related_products');
        $this->setDefaultSort('entity_id');
        $this->setUseAjax(true);
        if ($this->getCampaign() && $this->getCampaign()->getId()) {
            $this->setDefaultFilter(['in_products' => 1]);
        }
    }

    /**
     * @param Column $column
     * @return $this
     * @throws LocalizedException
     */
    protected function _addColumnFilterToCollection($column)
    {
        if ($column->getId() == 'in_products') {
            $productIds = $this->getSelectedProducts();
            if (empty($productIds)) {
                $productIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('entity_id', ['in' => $productIds]);
            } else {
                if ($productIds) {
                    $this->getCollection()->addFieldToFilter('entity_id', ['nin' => $productIds]);
                }
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }

    /**
     * @return Grid
     * @throws LocalizedException
     */
    protected function _prepareCollection()
    {
        $collection = $this->getRelatedProducts(false);
        $collection->addAttributeToSelect('*')
            ->joinField(
                'websites',
                'catalog_product_website',
                'website_id',
                'product_id=entity_id',
                null,
                'left'
            );
        $collection->groupByAttribute('entity_id');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * @return $this
     * @throws \Exception
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'in_products',
            [
                'type' => 'checkbox',
                'name' => 'in_products',
                'values' => $this->getSelectedProducts(),
                'align' => 'center',
                'index' => 'entity_id',
                'header_css_class' => 'col-select',
                'column_css_class' => 'col-select'
            ]
        );

        $this->addColumn(
            'entity_id',
            [
                'header' => __('ID'),
                'sortable' => true,
                'index' => 'entity_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );

        $this->addColumn(
            'name',
            [
                'header' => __('Name'),
                'index' => 'name',
                'header_css_class' => 'col-name',
                'column_css_class' => 'col-name'
            ]
        );

        $this->addColumn(
            'sku',
            [
                'header' => __('SKU'),
                'index' => 'sku',
                'header_css_class' => 'col-sku',
                'column_css_class' => 'col-sku'
            ]
        );

        $this->addColumn(
            'visibility',
            [
                'header' => __('Visibility'),
                'index' => 'visibility',
                'type' => 'options',
                'options' => $this->visibility->getOptionArray(),
                'header_css_class' => 'col-visibility',
                'column_css_class' => 'col-visibility'
            ]
        );

        if (!$this->_storeManager->isSingleStoreMode()) {
            $this->addColumn(
                'websites',
                [
                    'header' => __('Websites'),
                    'sortable' => false,
                    'index' => 'websites',
                    'type' => 'options',
                    'options' => $this->websiteFactory->create()->getCollection()->toOptionHash(),
                    'header_css_class' => 'col-websites',
                    'column_css_class' => 'col-websites'
                ]
            );
        }

        $this->addColumn(
            'status',
            [
                'header' => __('Status'),
                'index' => 'status',
                'type' => 'options',
                'options' => $this->status->getOptionArray(),
                'header_css_class' => 'col-status',
                'column_css_class' => 'col-status',
            ]
        );

        $this->addColumn(
            'position',
            [
                'header' => __('Position'),
                'name' => 'position',
                'type' => 'number',
                'validate_class' => 'validate-number',
                'index' => 'position',
                'editable' => true,
                'edit_only' => true,
                'sortable' => false,
                'filter' => false,
                'header_css_class' => 'no-display',
                'column_css_class' => 'no-display'
            ]
        );

        return parent::_prepareColumns();
    }
}
