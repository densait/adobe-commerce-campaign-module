<?php

namespace Denis\Campaign\Block\Adminhtml\Campaign\Edit\Tab;

use Denis\Campaign\Api\CampaignRepositoryInterface;
use Denis\Campaign\Block\Adminhtml\Campaign\Edit\Tab\RelatedProducts\Grid;
use Denis\Campaign\Model\Campaign;
use Magento\Backend\Block\Template;
use Magento\Backend\Block\Template\Context;
use Magento\Catalog\Block\Adminhtml\Category\Tab\Product;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Json\EncoderInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\BlockInterface;

/**
 * Class RelatedProducts
 */
class RelatedProducts extends Template
{
    /**
     * Block template
     *
     * @var string
     */
    protected $_template = 'campaign/related_products.phtml';

    /**
     * @var Product
     */
    protected $blockGrid;

    /**
     * @var EncoderInterface
     */
    protected $jsonEncoder;

    /**
     * @var CampaignRepositoryInterface
     */
    protected $campaignRepository;

    /**
     * @var Registry
     */
    protected $registry;

    /**
     * RelatedPosts constructor.
     * @param Context $context
     * @param Registry $registry
     * @param EncoderInterface $jsonEncoder
     * @param CampaignRepositoryInterface $campaignRepository
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        EncoderInterface $jsonEncoder,
        CampaignRepositoryInterface $campaignRepository,
        array $data = []
    ) {
        $this->registry = $registry;
        $this->jsonEncoder = $jsonEncoder;
        $this->campaignRepository = $campaignRepository;
        parent::__construct($context, $data);
    }

    /**
     * Return HTML of grid block
     *
     * @return string
     * @throws LocalizedException
     */
    public function getGridHtml()
    {
        return $this->getBlockGrid()->toHtml();
    }

    /**
     * Get current campaign
     *
     * @return Campaign
     */
    public function getCampaign()
    {
        return $this->registry->registry('campaign');
    }

    /**
     * Retrieve instance of grid block
     *
     * @return BlockInterface
     * @throws LocalizedException
     */
    public function getBlockGrid()
    {
        if (null === $this->blockGrid) {
            $this->blockGrid = $this->getLayout()->createBlock(
                Grid::class,
                'campaign.related.products'
            );
        }
        return $this->blockGrid;
    }

    /**
     * Get product data json
     *
     * @return string
     */
    public function getDataJson()
    {
        $related = [];
        if ($this->getCampaign()->getId()) {
            $related = $this->getCampaign()
                ->getResource()->getRelatedProductIds(
                    $this->getCampaign()->getId(),
                    true
                );
        }
        if (!empty($related)) {
            return $this->jsonEncoder->encode($related);
        }
        return '{}';
    }
}
