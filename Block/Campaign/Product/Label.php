<?php

namespace Denis\Campaign\Block\Campaign\Product;

use Denis\Campaign\Api\CampaignRepositoryInterface;
use Denis\Campaign\Api\Data\CampaignInterface;
use Magento\Catalog\Model\Product;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

/**
 * Class Label
 */
class Label extends Template
{
    /**
     * @var Registry
     */
    protected $coreRegistry;

    /**
     * @var CampaignRepositoryInterface
     */
    protected $campaignRepository;

    /**
     * @var string
     */
    protected $_template = 'campaign/product/label.phtml';

    /**
     * @var array
     */
    private $campaignsCache = [];

    /**
     * View constructor.
     * @param Context $context
     * @param Registry $coreRegistry
     * @param CampaignRepositoryInterface $campaignRepository
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        CampaignRepositoryInterface $campaignRepository,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->coreRegistry = $coreRegistry;
        $this->campaignRepository = $campaignRepository;
    }

    /**
     * Get product
     *
     * @return Product
     */
    public function getProduct()
    {
        if (!$this->hasData('product')) {
            $this->setData('product', $this->coreRegistry->registry('product'));
        }
        return $this->getData('product');
    }

    /**
     * Get product's campaigns
     *
     * @return CampaignInterface[]
     */
    public function getCampaigns()
    {
        if ($product = $this->getProduct()) {
            if (!array_key_exists($product->getId(), $this->campaignsCache)) {
                $this->campaignsCache[$product->getId()] = $this->campaignRepository
                    ->getCampaignsByProductIds(
                        [$product->getId()],
                        true,
                        $this->_storeManager->getStore()->getId()
                    );
            }
            return $this->campaignsCache[$product->getId()];
        }
        return [];
    }
}
