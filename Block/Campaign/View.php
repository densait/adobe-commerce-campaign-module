<?php

namespace Denis\Campaign\Block\Campaign;

use Denis\Campaign\Api\Data\CampaignInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Theme\Block\Html\Breadcrumbs;

/**
 * Class View
 */
class View extends Template
{
    const CACHE_TAG = 'eaa_attribute_option_view';

    /**
     * @var Registry
     */
    protected $coreRegistry;

    /**
     * View constructor.
     * @param Context $context
     * @param Registry $coreRegistry
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        array $data
    ) {
        $this->coreRegistry = $coreRegistry;
        parent::__construct($context, $data);
    }

    /**
     * Get current campaign
     *
     * @return CampaignInterface
     */
    public function getCampaign()
    {
        if (!$this->hasData('campaign')) {
            $this->setData('campaign', $this->coreRegistry->registry('current_campaign'));
        }
        return $this->getData('campaign');
    }

    /**
     * @param CampaignInterface $campaign
     * @throws LocalizedException
     * @return void
     */
    protected function _addBreadcrumbs(CampaignInterface $campaign)
    {
        /** @var Breadcrumbs $breadcrumbs */
        $breadcrumbs = $this->getLayout()->getBlock('breadcrumbs');
        if (!$breadcrumbs) {
            return;
        }

        $breadcrumbs->addCrumb(
            'home',
            [
                'label' => __('Home'),
                'title' => __('Home'),
                'link' => $this->_urlBuilder->getUrl(''),
            ]
        );

        $breadcrumbs->addCrumb(
            'denis_campaign',
            [
                'label' => __($campaign->getTitle()),
                'title' => __($campaign->getTitle()),
            ]
        );
    }

    /**
     * Prepare global layout
     *
     * @return $this
     * @throws LocalizedException
     */
    protected function _prepareLayout()
    {
        $campaign = $this->getCampaign();
        if (!$campaign) {
            return parent::_prepareLayout();
        }

        $this->_addBreadcrumbs($campaign);
        $this->pageConfig->getTitle()->set($campaign->getTitle());
        $this->pageConfig->setDescription($campaign->getDescription());

        $pageMainTitle = $this->getLayout()->getBlock('page.main.title');
        if ($pageMainTitle) {
            $pageMainTitle->setPageTitle($campaign->getTitle());
        }

        return parent::_prepareLayout();
    }

    /**
     * Render block HTML
     *
     * @return string
     */
    protected function _toHtml()
    {
        $option = $this->getCampaign();
        if (!$option) {
            return '';
        }
        return parent::_toHtml();
    }

    /**
     * Return identifiers for produced content
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getCampaign()->getId()];
    }
}
