<?php

namespace Denis\Campaign\Controller\Campaign;

use Denis\Campaign\Api\CampaignRepositoryInterface;
use Denis\Campaign\Api\Data\CampaignInterface;
use Denis\Campaign\Model\Layer\Campaign;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Api\Data\CategoryInterface;
use Magento\Catalog\Model\Layer\Resolver;
use Magento\Catalog\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\View\Result\Page;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;

/**
 * Class Index
 */
class View extends Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var CampaignRepositoryInterface
     */
    protected $campaignRepository;

    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var Session
     */
    protected $catalogSession;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var CategoryRepositoryInterface
     */
    protected $categoryRepository;

    /**
     * @var Resolver
     */
    protected $layerResolver;

    /**
     * Index constructor.
     *
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param CampaignRepositoryInterface $campaignRepository
     * @param Registry $registry
     * @param Session $catalogSession
     * @param StoreManagerInterface $storeManager
     * @param CategoryRepositoryInterface $categoryRepository
     * @param Resolver $layerResolver
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        CampaignRepositoryInterface $campaignRepository,
        Registry $registry,
        Session $catalogSession,
        StoreManagerInterface $storeManager,
        CategoryRepositoryInterface $categoryRepository,
        Resolver $layerResolver
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->campaignRepository = $campaignRepository;
        $this->registry = $registry;
        $this->catalogSession = $catalogSession;
        $this->storeManager = $storeManager;
        $this->categoryRepository = $categoryRepository;
        $this->layerResolver = $layerResolver;
    }

    /**
     * @return Page
     * @throws NotFoundException
     * @throws NoSuchEntityException
     */
    public function execute()
    {
        $page = $this->resultPageFactory->create();
        $campaign = $this->initCampaign();
        $category = $this->initCategory();
        if ($campaign && $category) {
            $this->layerResolver->create(Campaign::LAYER_NAME);
            $page->getConfig()->getTitle()->set($campaign->getTitle());
            return $page;
        }

        return $this->resultFactory
            ->create(ResultFactory::TYPE_FORWARD)
            ->forward('noroute');
    }

    /**
     * @return CampaignInterface|false
     */
    protected function initCampaign()
    {
        $campaignId = (int)$this->getRequest()->getParam('campaign_id');
        try {
            $campaign = $this->campaignRepository->getById($campaignId);
        } catch (LocalizedException $e) {
            return false;
        }

        if (!$campaign->isActive()) {
            return false;
        }

        $currentStores = [$this->storeManager->getStore()->getId(), Store::DEFAULT_STORE_ID];
        if (!count(array_intersect($campaign->getStoreId(), $currentStores))) {
            return false;
        }

        $this->registry->register('current_campaign', $campaign);
        return $campaign;
    }

    /**
     * Initialize requested category object
     *
     * @return CategoryInterface|false
     * @throws NoSuchEntityException
     */
    protected function initCategory()
    {
        $store = $this->storeManager->getStore();
        $categoryId = $store->getRootCategoryId();
        if (!$categoryId) {
            return false;
        }

        try {
            $category = $this->categoryRepository->get($categoryId, $store->getId());
        } catch (NoSuchEntityException $e) {
            return false;
        }

        $this->catalogSession->setLastVisitedCategoryId($category->getId());
        $this->registry->register('current_category', $category);
        try {
            $this->_eventManager->dispatch(
                'catalog_controller_category_init_after',
                ['category' => $category, 'controller_action' => $this]
            );
        } catch (LocalizedException $e) {
            $this->_objectManager->get(LoggerInterface::class)->critical($e);
            return false;
        }

        return $category;
    }
}
