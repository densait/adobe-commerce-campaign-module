<?php

namespace Denis\Campaign\Controller\Adminhtml\Campaign;

use Denis\Campaign\Api\CampaignRepositoryInterface;
use Denis\Campaign\Model\Campaign;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Backend\App\Action;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

/**
 * Edit Campaign action.
 */
class Edit extends Action implements HttpGetActionInterface
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Denis_Campaign::campaign_campaign';

    /**
     * Core registry
     *
     * @var Registry
     */
    protected $coreRegistry;

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var CampaignRepositoryInterface
     */
    protected $campaignRepository;

    /**
     * @param Action\Context $context
     * @param PageFactory $resultPageFactory
     * @param Registry $registry
     * @param CampaignRepositoryInterface $campaignRepository
     */
    public function __construct(
        Action\Context $context,
        PageFactory $resultPageFactory,
        Registry $registry,
        CampaignRepositoryInterface $campaignRepository
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->coreRegistry = $registry;
        $this->campaignRepository = $campaignRepository;
        parent::__construct($context);
    }

    /**
     * Init actions
     *
     * @return Page
     */
    protected function _initAction()
    {
        // load layout, set active menu and breadcrumbs
        /** @var Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Denis_Campaign::campaign_campaign')
            ->addBreadcrumb(__('Campaigns'), __('Campaigns'))
            ->addBreadcrumb(__('Manage Campaigns'), __('Manage Campaigns'));
        return $resultPage;
    }

    /**
     * Edit Campaign page
     *
     * @return Page
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('campaign_id');

        // 2. Initial checking
        if ($id) {
            try {
                $campaign = $this->campaignRepository->getById($id);
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage(__('This campaign no longer exists.'));
                /** \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        } else {
            $campaign = $this->_objectManager->create(Campaign::class);
        }

        $this->coreRegistry->register('campaign', $campaign);

        // 5. Build edit form
        /** @var Page $resultPage */
        $resultPage = $this->_initAction();
        $resultPage->addBreadcrumb(
            $id ? __('Edit Campaign') : __('New Campaign'),
            $id ? __('Edit Campaign') : __('New Campaign')
        );

        $resultPage->getConfig()->getTitle()->prepend(__('Campaigns'));
        $resultPage->getConfig()->getTitle()
            ->prepend($campaign->getId() ? $campaign->getTitle() : __('New Campaign'));

        return $resultPage;
    }
}
