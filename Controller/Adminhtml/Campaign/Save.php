<?php

namespace Denis\Campaign\Controller\Adminhtml\Campaign;

use Denis\Campaign\Api\CampaignRepositoryInterface;
use Denis\Campaign\Model\Campaign;
use Denis\Campaign\Model\CampaignFactory;
use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Serialize\SerializerInterface;

/**
 * Save Campaign action.
 */
class Save extends Action implements HttpPostActionInterface
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'enis_Campaign::campaign_campaign';

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var CampaignFactory
     */
    protected $campaignFactory;

    /**
     * @var CampaignRepositoryInterface
     */
    protected $campaignRepository;

    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * @param Action\Context $context
     * @param DataPersistorInterface $dataPersistor
     * @param CampaignFactory $campaignFactory
     * @param CampaignRepositoryInterface $campaignRepository
     * @param SerializerInterface $serializer
     */
    public function __construct(
        Action\Context $context,
        DataPersistorInterface $dataPersistor,
        CampaignFactory $campaignFactory,
        CampaignRepositoryInterface $campaignRepository,
        SerializerInterface $serializer
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->campaignFactory = $campaignFactory;
        $this->campaignRepository = $campaignRepository;
        $this->serializer = $serializer;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            if (isset($data['is_active']) && $data['is_active'] === 'true') {
                $data['is_active'] = Campaign::STATUS_ENABLED;
            }
            if (empty($data['campaign_id'])) {
                $data['campaign_id'] = null;
            }
            if (!empty($data['in_related_products'])) {
                $relatedProducts = $this->serializer->unserialize($data['in_related_products']);
                $data['related_product_ids'] = array_keys($relatedProducts);
            }

            /** @var Campaign $model */
            $model = $this->campaignFactory->create();
            $id = $this->getRequest()->getParam('campaign_id');
            if ($id) {
                try {
                    $model = $this->campaignRepository->getById($id);
                } catch (LocalizedException $e) {
                    $this->messageManager->addErrorMessage(__('This campaign no longer exists.'));
                    return $resultRedirect->setPath('*/*/');
                }
            }

            $model->setData($data);
            try {
                $this->campaignRepository->save($model);
                $this->messageManager->addSuccessMessage(__('You saved the campaign.'));
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath(
                        '*/*/edit',
                        ['campaign_id' => $model->getId()]
                    );
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addExceptionMessage($e->getPrevious() ?: $e);
            } catch (\Throwable $e) {
                $this->messageManager->addErrorMessage(__('Something went wrong while saving the campaign.'));
            }

            $this->dataPersistor->set('campaign', $data);
            return $resultRedirect->setPath(
                '*/*/edit',
                ['campaign_id' => $this->getRequest()->getParam('campaign_id')]
            );
        }
        return $resultRedirect->setPath('*/*/');
    }
}
