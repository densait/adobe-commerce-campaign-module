<?php

namespace Denis\Campaign\Controller\Adminhtml\Campaign;

use Denis\Campaign\Model\CampaignFactory;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Catalog\Controller\Adminhtml\Product;
use Magento\Catalog\Controller\Adminhtml\Product\Builder;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class Products
 */
class Products extends Product
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * Core registry
     *
     * @var Registry
     */
    protected $coreRegistry;

    /**
     * @var CampaignFactory
     */
    protected $campaignFactory;

    /**
     * AbstractAjaxGird constructor.
     *
     * @param Context $context
     * @param Builder $productBuilder
     * @param PageFactory $resultPageFactory
     * @param Registry $coreRegistry
     * @param CampaignFactory $campaignFactory
     */
    public function __construct(
        Context $context,
        Builder $productBuilder,
        PageFactory $resultPageFactory,
        Registry $coreRegistry,
        CampaignFactory $campaignFactory
    ) {
        parent::__construct($context, $productBuilder);
        $this->resultPageFactory = $resultPageFactory;
        $this->coreRegistry = $coreRegistry;
        $this->campaignFactory = $campaignFactory;
    }

    /**
     * Grid Data for AJAX request
     *
     * @return Page
     */
    public function execute()
    {
        $model = $this->campaignFactory->create();
        if ($id = $this->getRequest()->getParam('campaign_id')) {
            $model->load($id);
        }
        $this->coreRegistry->register('campaign', $model);
        $this->_view->loadLayout()->getLayout()->getBlock($this->getBlockName());
        $this->_view->renderLayout();
    }

    /**
     * @return string
     */
    protected function getBlockName()
    {
        return 'admin.denis_campaign.related_products.grid';
    }
}
