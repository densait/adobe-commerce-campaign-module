<?php

namespace Denis\Campaign\Controller\Adminhtml\Campaign;

use Denis\Campaign\Api\CampaignRepositoryInterface;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\Action\HttpPostActionInterface;

/**
 * Delete Campaign page action.
 */
class Delete extends Action implements HttpPostActionInterface
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Denis_Campaign::campaign_campaign';

    /**
     * @var CampaignRepositoryInterface
     */
    protected $campaignRepository;

    /**
     * Delete constructor.
     * @param Context $context
     * @param CampaignRepositoryInterface $campaignRepository
     */
    public function __construct(
        Context $context,
        CampaignRepositoryInterface $campaignRepository
    ) {
        parent::__construct($context);
        $this->campaignRepository = $campaignRepository;
    }

    /**
     * Delete action
     *
     * @return Redirect
     */
    public function execute()
    {
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('campaign_id');
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        if ($id) {
            try {
                $this->campaignRepository->deleteById($id);
                $this->messageManager->addSuccessMessage(__('The campaign has been deleted.'));
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                // display error message
                $this->messageManager->addErrorMessage($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['campaign_id' => $id]);
            }
        }

        // display error message
        $this->messageManager->addErrorMessage(__('We can\'t find a campaign to delete.'));

        // go to grid
        return $resultRedirect->setPath('*/*/');
    }
}
