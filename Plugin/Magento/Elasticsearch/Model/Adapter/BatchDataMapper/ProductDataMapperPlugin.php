<?php

namespace Denis\Campaign\Plugin\Magento\Elasticsearch\Model\Adapter\BatchDataMapper;

use Magento\Elasticsearch\Model\Adapter\BatchDataMapper\ProductDataMapper;

class ProductDataMapperPlugin
{
    /**
     * Map entity_id field to elasticSearch
     *
     * @param ProductDataMapper $subject
     * @param array|mixed $documents
     * @param mixed $documentData
     * @param mixed $storeId
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterMap(
        ProductDataMapper $subject,
        array $documents,
        array $documentData,
        int $storeId
    ): array {
        foreach ($documents as $productId => $document) {
            $document['entity_id'] = $productId;
            $documents[$productId] = $document;
        }
        return $documents;
    }
}
