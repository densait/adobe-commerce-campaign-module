<?php

namespace Denis\Campaign\Plugin\Magento\Catalog\Block\Product;

use Denis\Campaign\Block\Campaign\Product\Label;
use Magento\Catalog\Block\Product\AbstractProduct;
use Magento\Catalog\Model\Product;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class AbstractProductPlugin
 */
class AbstractProductPlugin
{
    /**
     * @param AbstractProduct $subject
     * @param string $result
     * @param Product $product
     * @return string
     * @throws LocalizedException
     */
    public function afterGetProductDetailsHtml(
        AbstractProduct $subject,
        $result,
        Product $product
    ) {
        $label = $subject->getLayout()
            ->createBlock(
                Label::class,
                'product.details.campaign.label.' . $product->getId()
            )
            ->setProduct($product)
            ->toHtml();

        return $result . $label;
    }
}
